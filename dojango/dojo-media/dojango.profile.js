var profile = (function(){
    return {
        basePath: "src/1.9.1",
        //releaseDir: "../../app",
        //releaseName: "lib",
        action: "release",
        layerOptimize: "closure",
        optimize: "closure",
        cssOptimize: "comments",
        mini: true,
        stripConsole: "all",
        selectorEngine: "lite",
 
        defaultConfig: {
            hasCache:{
                "dojo-built": 1,
                "dojo-loader": 1,
                "dom": 1,
                "host-browser": 1,
                "config-selectorEngine": "lite"
            },
            async: 1
        },
 
        staticHasFeatures: {
            "config-deferredInstrumentation": 0,
            "config-dojo-loader-catches": 0,
            "config-tlmSiblingOfDojo": 0,
            "dojo-amd-factory-scan": 0,
            "dojo-combo-api": 0,
            "dojo-config-api": 1,
            "dojo-config-require": 0,
            "dojo-debug-messages": 0,
            "dojo-dom-ready-api": 1,
            "dojo-firebug": 0,
            "dojo-guarantee-console": 1,
            "dojo-has-api": 1,
            "dojo-inject-api": 1,
            "dojo-loader": 1,
            "dojo-log-api": 0,
            "dojo-modulePaths": 0,
            "dojo-moduleUrl": 0,
            "dojo-publish-privates": 0,
            "dojo-requirejs-api": 0,
            "dojo-sniff": 1,
            "dojo-sync-loader": 0,
            "dojo-test-sniff": 0,
            "dojo-timeout-api": 0,
            "dojo-trace-api": 0,
            "dojo-undef-api": 0,
            "dojo-v1x-i18n-Api": 1,
            "dom": 1,
            "host-browser": 1,
            "extend-dojo": 1
        },

        plugins: {
                "xstyle/css": "xstyle/build/amd-css"
        },

        packages:[{
            name: "dojo",
            location: "dojo"
        },{
            name: "dijit",
            location: "dijit"
        },{
            name: "dojox",
            location: "dojox"
        },{
            name: "put-selector",
            location: "put-selector"
        },{
            name: "xstyle",
            location: "xstyle",
            trees: [
                // don't bother with .hidden, tests and txt.
                [".", ".", /(\/\.)|(~$)|(test|txt)|(build)/]
            ]
        },{
            name: "dgrid",
            location: "dgrid",
            trees: [
                // don't bother with .hidden, tests and txt.
                [".", ".", /(\/\.)|(~$)|(test|txt)/]
            ]
        }
        ],
 
        layers: {
            "dojo/dojo": {
                include: [
                    "dojo/dojo",
                    "dojo/i18n",
                    "dojo/domReady",
                    "dojo/dom-construct",
                    "dojo/cookie",
                    "dojo/ready",
                    "dojo/main",
                    "dojo/io-query",
                    "dojo/store/Memory",
                    "dojo/store/Cache",
                    "dojo/store/Observable",
                    "dojo/store/JsonRest",
                    "dojo/data/ItemFileReadStore",
                    "dojo/data/ObjectStore",
                    "dojo/date/locale",
                    "dojo/cldr/nls/gregorian",
                    ],
                customBase: true,
                boot: true
            },
            "dijit/dijit": {
                include: [
                    "dijit/registry",
                    "dijit/TitlePane",
                    "dijit/Dialog",
                    "dijit/Menu",
                    "dijit/form/Form",
                    "dijit/form/TextBox",
                    "dijit/form/Button",
                    "dijit/form/Select",
                    "dijit/form/FilteringSelect",
                    "dijit/form/ValidationTextBox",
                    "dijit/nls/loading",
                    "dijit/nls/common",
                    "dijit/nls/it/loading",
                    "dijit/nls/it/common",
                    "dijit/form/ComboBox",
                    "dijit/form/DateTextBox",
                    "dijit/form/CurrencyTextBox", 
                    "dijit/form/nls/validate",
                    "dijit/form/nls/ComboBox",
                    "dijit/form/nls/it/validate",
                    "dijit/form/nls/it/ComboBox",
                    "dijit/layout/_LayoutWidget",
                    "dijit/layout/AccordionContainer",
                    "dijit/layout/StackContainer"
                ]
            },
            "dojox/dojox": {
                include: [
                    "dojox/gfx/path",
                    "dojox/gfx/svg",
                    "dojox/validate",
                    "dojox/timing",
                    "dojox/charting/Chart",
                    "dojox/charting/StoreSeries",
                    "dojox/charting/themes/Julie",
                    "dojox/charting/plot2d/Pie",
                    "dojox/charting/plot2d/Lines",
                    "dojox/charting/plot2d/Markers",
                    "dojox/charting/widget/Legend",
                    "dojox/charting/widget/SelectableLegend",
                    "dojox/charting/action2d/Tooltip",
                    "dojox/charting/action2d/MoveSlice",
                    "dojox/charting/action2d/Magnify",
                    "dojox/charting/action2d/Highlight",
                    "dojox/charting/axis2d/Default",
                    "dojox/charting/plot2d/StackedAreas",
                    "dojox/charting/plot2d/StackedLines",
                    "dojox/charting/plot2d/Grid",
                    "dojox/charting/plot2d/Indicator",
                    "dojox/charting/plot2d/MarkersOnly", 
                    "dojox/charting/action2d/Magnify",
                    "dojox/charting/action2d/Tooltip",
                    "dojox/widget/DialogSimple"
                ]
            },

            'dgrid/dgrid': {
                include: ["dgrid/List", "dgrid/OnDemandGrid", "dgrid/Selection",
                "dgrid/Keyboard"]
            }
        }
    };
})();

/*
dependencies = {
	layers: [
		{
			name: "dojo.js",
			dependencies: [
				"dojango.dojango",
				"dojo.parser"
			]
		}
	],
	
	prefixes: [
		[ "dijit", "../dijit" ],
		[ "dojox", "../dojox" ],
                //[ "dgrid", "../dgrid" ],
                [ "put-selector", "../put-selector"],
                [ "dbind", "../dbind"],
                [ "build", "../util/build"],
                [ "xstyle", "../xstyle" ],
		[ "dojango", "../../../dojango" ] // relative to the directory, where the dojo.js source file resides
	]
}
*/
